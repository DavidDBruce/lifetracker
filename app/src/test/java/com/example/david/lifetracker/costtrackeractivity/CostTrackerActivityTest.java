package com.example.david.lifetracker.costtrackeractivity;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.example.david.lifetracker.cost.Cost;
import com.example.david.lifetracker.payment.Payment;
import com.example.david.lifetracker.utility.SQLDB;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class CostTrackerActivityTest {

    @Mock
    private SQLDB sqldb;
    @Mock
    private SharedPreferences sharedPreferences;

    @InjectMocks
    private CostTrackerActivity classToTest = new CostTrackerActivity();

    @Test
    public void totalSaved() {
        when(sqldb.retrieveAllCosts()).thenReturn(Arrays.asList(
                getCost(BigDecimal.valueOf(100, 2),
                        "Food",
                        new DateTime(1),
                        1,
                        false,
                        0,
                        "Hy-Vee",
                        BigDecimal.valueOf(100, 2),
                        "One-Time"),
                getCost(BigDecimal.valueOf(200, 2),
                        "Gas",
                        new DateTime(2),
                        2,
                        false,
                        0,
                        "Casey's",
                        BigDecimal.valueOf(200, 2),
                        "One-Time")));
        assertEquals(BigDecimal.valueOf(300, 2), classToTest.totalSaved());
    }

    @Test
    public void performAnnualCostInPayPeriod() {
        when(sharedPreferences.edit()).thenReturn(new TestEditor());
        classToTest.performAnnualCostInPayPeriod(getCost(
                BigDecimal.valueOf(100, 2),
                "Food",
                new DateTime(1),
                1,
                false,
                0,
                "Hy-Vee",
                BigDecimal.valueOf(100, 2),
                "One-Time"
        ), BigDecimal.valueOf(300, 2));

        verify(sqldb).createPayment(any(Payment.class));
        verify(sqldb).updateCost(any(Cost.class));
        verify(sharedPreferences).edit();
    }

    //--------------------------
    //------Helper Methods------
    //--------------------------

    private Cost getCost(BigDecimal amount, String category, DateTime date, int id, boolean isMonthPaid, int monthPaid, String name, BigDecimal saved, String type) {
        Cost cost = new Cost();
        cost.setAmount(amount);
        cost.setCategory(category);
        cost.setDate(date);
        cost.setId(id);
        cost.setIsMonthPaid(isMonthPaid);
        cost.setMonthPaid(monthPaid);
        cost.setName(name);
        cost.setSaved(saved);
        cost.setType(type);
        return cost;
    }
}

class TestEditor implements SharedPreferences.Editor {

    @Override
    public SharedPreferences.Editor putString(String key, @Nullable String value) {
        return null;
    }

    @Override
    public SharedPreferences.Editor putStringSet(String key, @Nullable Set<String> values) {
        return null;
    }

    @Override
    public SharedPreferences.Editor putInt(String key, int value) {
        return null;
    }

    @Override
    public SharedPreferences.Editor putLong(String key, long value) {
        return null;
    }

    @Override
    public SharedPreferences.Editor putFloat(String key, float value) {
        return null;
    }

    @Override
    public SharedPreferences.Editor putBoolean(String key, boolean value) {
        return null;
    }

    @Override
    public SharedPreferences.Editor remove(String key) {
        return null;
    }

    @Override
    public SharedPreferences.Editor clear() {
        return null;
    }

    @Override
    public boolean commit() {
        return false;
    }

    @Override
    public void apply() {

    }
}