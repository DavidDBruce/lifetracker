package com.example.david.lifetracker.utility;

import org.joda.time.DateTime;
import org.junit.Test;

import static com.example.david.lifetracker.utility.Utilities.getPayPeriods;
import static org.junit.Assert.assertEquals;

public class UtilitiesTest {
    @Test
    public void getPayPeriods_3_twoWeeksPlusOneDay() {
        assertEquals(3, getPayPeriods(getDateTime(6, 16, 2018), getDateTime(6, 1, 2018)));
    }

    @Test
    public void getPayPeriods_2_twoWeeksExactly() {
        assertEquals(2, getPayPeriods(getDateTime(6, 15, 2018), getDateTime(6, 1, 2018)));
    }

    @Test
    public void getPayPeriods_2_justUnderTwoWeeks() {
        assertEquals(2, getPayPeriods(getDateTime(6, 14, 2018), getDateTime(6, 1, 2018)));
    }

    @Test
    public void getPayPeriods_1_sameDay() {
        assertEquals(1, getPayPeriods(getDateTime(6, 1, 2018), getDateTime(6, 1, 2018)));
    }

    @Test
    public void getPayPeriods_1_late() {
        assertEquals(1, getPayPeriods(getDateTime(5, 15, 2018), getDateTime(6, 1, 2018)));
    }

    private DateTime getDateTime(int month, int day, int year) {
        return new DateTime(year, month, day, 12, 0, 0);
    }
}