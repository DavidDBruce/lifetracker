package com.example.david.lifetracker.pay;

import org.joda.time.DateTime;

import java.math.BigDecimal;

public class Pay {

    private int id;
    private BigDecimal amount;
    private String type;
    private DateTime date;


    public Pay() {
        id = -1;
        amount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
        type = "";
        date = DateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}