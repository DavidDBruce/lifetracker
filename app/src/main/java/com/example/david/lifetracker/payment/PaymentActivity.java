package com.example.david.lifetracker.payment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.utility.SQLDB;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Locale;

import static java.lang.String.format;

public class PaymentActivity extends AppCompatActivity {

    Intent intent;
    int sqlId;
    private Spinner paymentCategorySPN;
    private EditText paymentNameET;
    private EditText paymentAmountET;
    private TextView paymentDateTV;
    private DatePickerDialog.OnDateSetListener date;
    private Calendar myCalendar;
    private SQLDB sqldb;

    private void init() {
        sqldb = new SQLDB(getApplicationContext());

        paymentCategorySPN = findViewById(R.id.paymentCategorySPN);

        paymentNameET = findViewById(R.id.paymentNameET);
        paymentAmountET = findViewById(R.id.paymentAmountET);
        paymentDateTV = findViewById(R.id.paymentDateInputTV);

        intent = getIntent();
        sqlId = intent.getIntExtra("SQL", -1);

        myCalendar = Calendar.getInstance();

        date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateLabel();
        };

        paymentDateTV.setOnClickListener(v -> new DatePickerDialog(PaymentActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentCategorySPN.setAdapter(categoryAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);

        init();

        if (sqlId != -1) {
            buildFromDB();
        }
    }

    public void save(View v) {
        Payment payment = new Payment();
        payment.setName(paymentNameET.getText().toString());

        if (!paymentAmountET.getText().toString().isEmpty()) {
            payment.setAmount(BigDecimal.valueOf(Double.parseDouble(paymentAmountET.getText().toString().replace("$", ""))));
        } else {
            payment.setAmount(BigDecimal.ZERO);
        }

        payment.setDate(new DateTime(myCalendar.getTimeInMillis()));
        payment.setCategory(paymentCategorySPN.getSelectedItem().toString());

        if (sqlId == -1) {
            sqldb.createPayment(payment);
        } else {
            payment.setId(sqlId);
            sqldb.updatePayment(payment);
        }

        finish();
    }

    public void delete(View v) {
        new AlertDialog.Builder(PaymentActivity.this)
                .setTitle("Delete Payment")
                .setMessage("Are you sure you want to delete this payment?")
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    if (sqlId == -1) {
                        finish();
                    } else {
                        sqldb.deletePayment(sqlId);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> {
                    //Cancel user action
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void updateDateLabel() {
        DateTime dateTime = new DateTime(myCalendar.getTime());
        paymentDateTV.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));
    }

    private void buildFromDB() {
        try {
            Payment payment = sqldb.retrievePaymentByID(sqlId);
            paymentNameET.setText(payment.getName());
            paymentAmountET.setText(format(Locale.US, "$%s", payment.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP)));
            myCalendar.setTimeInMillis(payment.getDate().getMillis());
            DateTime dateTime = new DateTime(myCalendar.getTime());
            paymentDateTV.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));

            String[] categories = getResources().getStringArray(R.array.categories);
            for (int i = 0; i < categories.length; i++) {
                if (payment.getCategory().equals(categories[i])) {
                    paymentCategorySPN.setSelection(i);
                    break;
                }
            }

        } catch (Exception e) {
            Log.e("Payment Error", e.getMessage());
        }
    }
}