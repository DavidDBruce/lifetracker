package com.example.david.lifetracker.cost;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Comparator;


public class Cost implements Comparator<Cost>, Comparable<Cost> {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("amount")
    private BigDecimal amount;
    @SerializedName("type")
    private String type;
    @SerializedName("date")
    private DateTime date;
    @SerializedName("category")
    private String category;
    @SerializedName("saved")
    private BigDecimal saved;
    @SerializedName("monthPaid")
    private boolean monthPaid;

    public Cost() {
        id = -1;
        name = "";
        amount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
        type = "";
        date = DateTime.now();
        saved = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
        monthPaid = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getSaved() {
        return saved;
    }

    public void setSaved(BigDecimal saved) {
        this.saved = saved;
    }

    public int getMonthPaid() {
        return monthPaid ? 1 : 0;
    }

    public void setMonthPaid(int monthPaid) {
        this.monthPaid = monthPaid == 1;
    }

    public void setIsMonthPaid(boolean monthPaid) {
        this.monthPaid = monthPaid;
    }

    public boolean isMonthPaid() {
        return monthPaid;
    }

    @Override
    public int compareTo(@NonNull Cost c) {
        return Long.compare(date.getMillis(), c.getDate().getMillis());
    }

    @Override
    public int compare(Cost leftCost, Cost rightCost) {
        if (leftCost.getDate().getMillis() < rightCost.getDate().getMillis()) {
            return -1;
        } else if (leftCost.getDate() == rightCost.getDate()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        Cost cost = null;
        if (o instanceof Cost) {
            cost = (Cost) o;
        }

        if (this == cost) {
            return true;
        }

        return EqualsBuilder.reflectionEquals(this, cost, true);
    }

    @NonNull
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, true);
    }
}