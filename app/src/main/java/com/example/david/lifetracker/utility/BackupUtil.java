package com.example.david.lifetracker.utility;

import android.content.Context;
import android.content.Intent;

import com.example.david.lifetracker.payment.Payment;

import org.joda.time.DateTime;

import java.util.List;

import static com.example.david.lifetracker.utility.Utilities.GSON;

public class BackupUtil {

    private SQLDB sqldb;
    private DateTime now = DateTime.now();
    private Context context;
    private static final String TAG = "Debug TAG";

    BackupUtil(Context context) {
        this.context = context;
        sqldb = new SQLDB(context);
    }

    Intent createIntentToBackupCostsPureText() {
        return createPureTextIntent(GSON.toJson(sqldb.retrieveAllCosts()), "All Costs JSON");
    }

    Intent createIntentToBackupPaymentsPureText() {
        List<Payment> payments = sqldb.retrieveAllPayments();
        String json = GSON.toJson(payments);
        return createPureTextIntent(json, "All Payments JSON");
    }

    private Intent createPureTextIntent(String text, String subject) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        String[] to = {"daviddouglasbruce@gmail.com"};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        return emailIntent;
    }
}
