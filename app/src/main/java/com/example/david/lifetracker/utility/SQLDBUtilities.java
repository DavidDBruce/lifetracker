package com.example.david.lifetracker.utility;

class SQLDBUtilities {
    static final String CREATE_TABLE_IF_NOT_EXISTS = "CREATE TABLE IF NOT EXISTS ";
    static final String INTEGER_PRIMARY_KEY_AUTOINCREMENT = " INTEGER PRIMARY KEY AUTOINCREMENT, ";
    static final String TEXT = " TEXT,";
    static final String DECIMAL = " DECIMAL,";
    static final String INT8 = " INT8,";
    static final String SELECT_FROM = "SELECT * FROM ";
    static final String WHERE = " WHERE ";
    static final String DB_READ_ERROR = "DB Read Error";

    private SQLDBUtilities() {
        //Not to be instantiated.
    }


}
