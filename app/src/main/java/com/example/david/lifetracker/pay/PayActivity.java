package com.example.david.lifetracker.pay;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.utility.SQLDB;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Locale;

import static java.lang.String.format;

public class PayActivity extends AppCompatActivity {

    Intent intent;
    int sqlId;
    private Spinner payTypeSPN;
    private EditText payAmountET;
    private TextView payDateTV;
    private DatePickerDialog.OnDateSetListener date;
    private Calendar myCalendar;
    private SQLDB sqldb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_activity);

        initialize();

        if (sqlId != -1) {
            buildFromDB();
        }
    }

    public void save(View v) {

        Pay pay = new Pay();

        if (!payAmountET.getText().toString().isEmpty()) {
            pay.setAmount(BigDecimal.valueOf(Double.parseDouble(payAmountET.getText().toString().replace("$", ""))));
        } else {
            pay.setAmount(BigDecimal.ZERO);
        }
        pay.setDate(new DateTime(myCalendar.getTimeInMillis()));
        pay.setType(payTypeSPN.getSelectedItem().toString());

        if (sqlId == -1) {
            sqldb.createPay(pay);
        } else {
            pay.setId(sqlId);
            sqldb.updatePay(pay);
        }

        finish();
    }

    public void delete(View v) {
        new AlertDialog.Builder(PayActivity.this)
                .setTitle("Delete Paycheck")
                .setMessage("Are you sure you want to delete this Paycheck?")
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    if (sqlId == -1) {
                        finish();
                    } else {
                        sqldb.deletePay(sqlId);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> {
                    //Cancel user action
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    private void updateDateLabel() {
        DateTime dateTime = new DateTime(myCalendar.getTime());
        payDateTV.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));
    }

    private void buildFromDB() {
        try {
            Pay pay = sqldb.retrievePayByID(sqlId);
            payAmountET.setText(format(Locale.US, "$%s", pay.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
            myCalendar.setTimeInMillis(pay.getDate().getMillis());
            DateTime dateTime = new DateTime(myCalendar.getTime());
            payDateTV.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));

            String[] payTypes = getResources().getStringArray(R.array.pay_types);
            for (int i = 0; i < payTypes.length; i++) {

                if (pay.getType().equals(payTypes[i])) {
                    payTypeSPN.setSelection(i);
                    break;
                }

            }

        } catch (Exception e) {
            Log.e("PayActivity Error", e.getMessage());
        }
    }

    private void initialize() {
        sqldb = new SQLDB(getApplicationContext());

        payTypeSPN = findViewById(R.id.payTypeSPN);

        payAmountET = findViewById(R.id.payAmountET);
        payDateTV = findViewById(R.id.payDateInputTV);

        intent = getIntent();
        sqlId = intent.getIntExtra("SQL", -1);

        myCalendar = Calendar.getInstance();

        date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateLabel();
        };

        payDateTV.setOnClickListener(v -> new DatePickerDialog(PayActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this,
                R.array.pay_types, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        payTypeSPN.setAdapter(typeAdapter);
    }
}