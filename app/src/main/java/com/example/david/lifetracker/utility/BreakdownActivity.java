package com.example.david.lifetracker.utility;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.payment.Payment;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.david.lifetracker.utility.Utilities.DATE_FORMAT;
import static com.example.david.lifetracker.utility.Utilities.addAmounts;
import static com.example.david.lifetracker.utility.Utilities.compareDatesToTheDay;
import static com.example.david.lifetracker.utility.Utilities.displayShortToast;
import static java.lang.String.format;

public class BreakdownActivity extends AppCompatActivity {

    GraphView costGV;
    SQLDB sqldb;
    TextView startDateInputTV;
    TextView endDateInputTV;
    TextView costSummaryBreakdownTV;
    DateTime startDate;
    DateTime endDate;

    private DatePickerDialog.OnDateSetListener datePickerDialog;
    private Calendar calendar;

    boolean updateStart = false;
    boolean updateEnd = false;

    public BreakdownActivity() {
    }

    public BreakdownActivity(GraphView graphView, //NOSONAR
                             SQLDB sqldb,
                             TextView startDateInputTV,
                             TextView endDateInputTV,
                             TextView costSummaryBreakdownTV,
                             DateTime startDate,
                             DateTime endDate,
                             DatePickerDialog.OnDateSetListener datePickerDialog,
                             Calendar calendar,
                             boolean updateStart,
                             boolean updateEnd) {
        this.costGV = graphView;
        this.sqldb = sqldb;
        this.startDateInputTV = startDateInputTV;
        this.endDateInputTV = endDateInputTV;
        this.costSummaryBreakdownTV = costSummaryBreakdownTV;
        this.startDate = startDate;
        this.endDate = endDate;
        this.datePickerDialog = datePickerDialog;
        this.calendar = calendar;
        this.updateStart = updateStart;
        this.updateEnd = updateEnd;
    }

    private void init() {
        startDate = DateTime.now();
        endDate = DateTime.now();

        sqldb = new SQLDB(getApplicationContext());

        costGV = findViewById(R.id.costGV);

        startDateInputTV = findViewById(R.id.breakdownStartDateInputTV);
        startDateInputTV.setText(format(Locale.US, DATE_FORMAT, startDate.getMonthOfYear(), startDate.getDayOfMonth(), startDate.getYear()));

        endDateInputTV = findViewById(R.id.breakdownEndDateInputTV);
        endDateInputTV.setText(format(Locale.US, DATE_FORMAT, startDate.getMonthOfYear(), startDate.getDayOfMonth(), startDate.getYear()));

        costSummaryBreakdownTV = findViewById(R.id.costSummaryBreakdownTV);
        costSummaryBreakdownTV.setText(createReport());
        costSummaryBreakdownTV.setOnClickListener(v -> costSummaryBreakdownTV.setText(createReport()));

        calendar = Calendar.getInstance();

        datePickerDialog = createDatePickerDialog();

        startDateInputTV.setOnClickListener(createStartOnClickListener());

        endDateInputTV.setOnClickListener(createEndOnClickListener());
    }

    private DatePickerDialog.OnDateSetListener createDatePickerDialog() {
        return (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            updateDateLabel();
            buildGV();
            costSummaryBreakdownTV.setText(createReport());
        };
    }

    private View.OnClickListener createStartOnClickListener() {
        return v -> {
            new DatePickerDialog(BreakdownActivity.this,
                    datePickerDialog,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH))
                    .show();
            updateStart = true;
        };
    }

    private View.OnClickListener createEndOnClickListener() {
        return v -> {
            new DatePickerDialog(BreakdownActivity.this,
                    datePickerDialog,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH))
                    .show();
            updateEnd = true;
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.breakdown_activity);
        init();
        buildGV();
        createReport();
    }

    private void buildGV() {

        costGV.removeAllSeries();

        Map<String, BigDecimal> totals = new HashMap<>();
        List<Payment> payments = new ArrayList<>();
        try {
            sqldb.retrieveAllPayments().stream()
                    .filter(this::isInDateRange)
                    .forEach(payments::add);
        } catch (Exception e) {
            displayShortToast(getApplicationContext(), "DB Error");
            return;
        }

        if (payments.isEmpty()) {
            return;
        }

        payments.forEach(payment -> totals.put(payment.getCategory(), Optional.ofNullable(totals.get(payment.getCategory())).orElse(BigDecimal.ZERO).add(payment.getAmount())));

        List<DataPoint> dataPoints = new ArrayList<>();
        String[] names = new String[totals.size()];
        int index = 0;

        for (Map.Entry<String, BigDecimal> name : totals.entrySet()) {
            dataPoints.add(new DataPoint(index, name.getValue().doubleValue()));
            names[index] = name.getKey();
            index++;
        }

        if (!dataPoints.isEmpty()) {
            BarGraphSeries<DataPoint> costsSeries = new BarGraphSeries<>(dataPointListToArray(dataPoints));
            costsSeries.setSpacing(50);
            costsSeries.setDrawValuesOnTop(true);
            costsSeries.setValuesOnTopColor(Color.BLACK);
            costsSeries.setValuesOnTopSize(50);

            costGV.addSeries(costsSeries);

            costGV.getViewport().setXAxisBoundsManual(true);
            costGV.getViewport().setMinX(0);
            costGV.getViewport().setMaxX(dataPoints.size() - 1.0);

            costGV.getViewport().setYAxisBoundsManual(true);
            costGV.getViewport().setMinY(0);
            costGV.getViewport().setMaxY(dataPoints.get(maxDataPoint(dataPoints)).getY());

            if (names.length > 1) {
                StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(costGV);
                staticLabelsFormatter.setHorizontalLabels(names);
                costGV.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
            }
        }
    }

    private DataPoint[] dataPointListToArray(List<DataPoint> dataPointList) {
        DataPoint[] dataPointArray = new DataPoint[dataPointList.size()];
        for (int i = 0; i < dataPointArray.length; i++) {
            dataPointArray[i] = dataPointList.get(i);
        }
        return dataPointArray;
    }

    private boolean isInDateRange(Payment payment) {
        return compareDatesToTheDay(payment.getDate(), startDate) >= 0 && compareDatesToTheDay(payment.getDate(), endDate) < 0;
    }

    private void updateDateLabel() {
        if (updateStart) {
            startDate = new DateTime(calendar.getTime());
            startDateInputTV.setText(format(Locale.US, DATE_FORMAT, startDate.getMonthOfYear(), startDate.getDayOfMonth(), startDate.getYear()));
            updateStart = false;
        }
        if (updateEnd) {
            endDate = new DateTime(calendar.getTime());
            endDateInputTV.setText(format(Locale.US, DATE_FORMAT, endDate.getMonthOfYear(), endDate.getDayOfMonth(), endDate.getYear()));
            updateEnd = false;
        }
    }

    private int maxDataPoint(List<DataPoint> dataPoints) {
        int returnIndex = 0;
        double largestDataPoint = 0;

        for (int i = 0; i < dataPoints.size(); i++) {
            if (largestDataPoint < dataPoints.get(i).getY()) {
                largestDataPoint = dataPoints.get(i).getY();
                returnIndex = i;
            }
        }

        return returnIndex;
    }

    private String createReport() {
        String returnReport;
        List<Payment> paymentList;
        try {
            paymentList = sqldb.retrieveAllPayments();
        } catch (Exception e) {
            Log.e("Breakdown Error", e.getMessage());
            return "Breakdown Error";
        }

        BigDecimal coffeeSpending = addAmounts(paymentList.stream()
                .filter(payment -> "Coffee".equals(payment.getName()))
                .map(Payment::getAmount)
                .collect(Collectors.toCollection(ArrayList::new)));

        BigDecimal overallSpending = addAmounts(paymentList.stream()
                .filter(payment -> !"Coffee".equals(payment.getName()))
                .map(Payment::getAmount)
                .collect(Collectors.toCollection(ArrayList::new)));

        returnReport = format("You spent %s on coffee.%sYou spent %s overall.", format(Locale.US, "$%s", coffeeSpending.setScale(2, BigDecimal.ROUND_HALF_UP)), '\n', format(Locale.US, "$%s", overallSpending.setScale(2, BigDecimal.ROUND_HALF_UP)));

        return returnReport;
    }
}
