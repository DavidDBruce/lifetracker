package com.example.david.lifetracker.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.david.lifetracker.cost.Cost;
import com.example.david.lifetracker.pay.Pay;
import com.example.david.lifetracker.payment.Payment;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.example.david.lifetracker.utility.SQLDBUtilities.CREATE_TABLE_IF_NOT_EXISTS;
import static com.example.david.lifetracker.utility.SQLDBUtilities.DB_READ_ERROR;
import static com.example.david.lifetracker.utility.SQLDBUtilities.DECIMAL;
import static com.example.david.lifetracker.utility.SQLDBUtilities.INT8;
import static com.example.david.lifetracker.utility.SQLDBUtilities.INTEGER_PRIMARY_KEY_AUTOINCREMENT;
import static com.example.david.lifetracker.utility.SQLDBUtilities.SELECT_FROM;
import static com.example.david.lifetracker.utility.SQLDBUtilities.TEXT;
import static com.example.david.lifetracker.utility.SQLDBUtilities.WHERE;
import static java.lang.String.format;
import static java.lang.String.valueOf;

public class SQLDB extends SQLiteOpenHelper {

    // Table Names
    private static final String TABLE_CO = "cost";
    private static final String TABLE_PA = "paycheck";
    private static final String TABLE_PM = "payment";

    //Column Names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_TYPE = "type";
    private static final String KEY_DATE = "date";
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_SAVED = "saved";
    private static final String KEY_MONTH_PAID = "monthpaid";

    private static final String DATABASE_ALTER_COST_V2_SAVED = format("ALTER TABLE %s ADD COLUMN %s DECIMAL DEFAULT 0", TABLE_CO, KEY_SAVED);
    private static final String DATABASE_ALTER_COST_V2_PAID = format("ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT 0", TABLE_CO, KEY_MONTH_PAID);

    //DBInfo
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "CostTrackerDB";

    private static final String ID_EQUALS_ANY = "%s = ?";

    public SQLDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createCOTable(db);
        createPATable(db);
        createPMTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            db.execSQL(DATABASE_ALTER_COST_V2_SAVED);
            db.execSQL(DATABASE_ALTER_COST_V2_PAID);
        }
    }

    //Cost Table

    //Create Cost Table
    private void createCOTable(SQLiteDatabase db) {
        String CREATE_CO_TABLE = CREATE_TABLE_IF_NOT_EXISTS + TABLE_CO + " ( "
                + KEY_ID + INTEGER_PRIMARY_KEY_AUTOINCREMENT
                + KEY_NAME + TEXT
                + KEY_AMOUNT + DECIMAL
                + KEY_DATE + INT8
                + KEY_TYPE + TEXT
                + KEY_CATEGORY + TEXT
                + KEY_SAVED + DECIMAL
                + KEY_MONTH_PAID + " INTEGER"
                + " )";
        db.execSQL(CREATE_CO_TABLE);
    }

    //Create Cost
    public void createCost(Cost co) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues curEntry = new ContentValues();

        curEntry.put(KEY_NAME, co.getName());
        curEntry.put(KEY_AMOUNT, co.getAmount().doubleValue());
        curEntry.put(KEY_DATE, co.getDate().getMillis());
        curEntry.put(KEY_TYPE, co.getType());
        curEntry.put(KEY_CATEGORY, co.getCategory());
        curEntry.put(KEY_SAVED, co.getSaved().doubleValue());
        curEntry.put(KEY_MONTH_PAID, co.getMonthPaid());

        db.insert(TABLE_CO, null, curEntry);
        db.close();
    }

    //Retrieve CO by ID
    public Cost retrieveCostByID(int SQL_ID) {

        String query = SELECT_FROM + TABLE_CO + WHERE + KEY_ID + " = " + SQL_ID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Cost co = new Cost();
        if (cursor.moveToFirst()) {
            co.setId(Integer.parseInt(cursor.getString(0)));
            co.setName(cursor.getString(1));
            co.setAmount(BigDecimal.valueOf(cursor.getDouble(2)));
            co.setDate(new DateTime(cursor.getLong(3)));
            co.setType(cursor.getString(4));
            co.setCategory(cursor.getString(5));
            co.setSaved(BigDecimal.valueOf(cursor.getDouble(6)));
            co.setMonthPaid(cursor.getInt(7));
            return co;
        } else {
            co.setName(DB_READ_ERROR);
        }

        cursor.close();

        return co;

    }

    //Retrieve COs
    public List<Cost> retrieveAllCosts() {
        List<Cost> cos = new ArrayList<>();

        String query = SELECT_FROM + TABLE_CO;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Cost co;
        if (cursor.moveToFirst()) {
            do {
                co = new Cost();
                co.setId(Integer.parseInt(cursor.getString(0)));
                co.setName(cursor.getString(1));
                co.setAmount(BigDecimal.valueOf(cursor.getDouble(2)));
                co.setDate(new DateTime(cursor.getLong(3)));
                co.setType(cursor.getString(4));
                co.setCategory(cursor.getString(5));
                co.setSaved(BigDecimal.valueOf(cursor.getDouble(6)));
                co.setMonthPaid(cursor.getInt(7));
                cos.add(co);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return cos;
    }

    //Update CO
    public int updateCost(Cost co) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues curEntry = new ContentValues();

        curEntry.put(KEY_NAME, co.getName());
        curEntry.put(KEY_AMOUNT, co.getAmount().doubleValue());
        curEntry.put(KEY_DATE, co.getDate().getMillis());
        curEntry.put(KEY_TYPE, co.getType());
        curEntry.put(KEY_CATEGORY, co.getCategory());
        curEntry.put(KEY_SAVED, co.getSaved().doubleValue());
        curEntry.put(KEY_MONTH_PAID, co.getMonthPaid());

        int i = db.update(TABLE_CO, curEntry, format(ID_EQUALS_ANY, KEY_ID), new String[]{valueOf(co.getId())});

        db.close();

        return i;

    }

    //Delete CO
    public void deleteCost(int SQL_ID) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_CO, format(ID_EQUALS_ANY, KEY_ID), new String[]{valueOf(SQL_ID)});

        db.close();
    }

    public void clearCosts() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS cost");
        onCreate(db);
    }

    //Pay Table

    //Create Pay Table
    private void createPATable(SQLiteDatabase db) {
        String CREATE_PA_TABLE = CREATE_TABLE_IF_NOT_EXISTS + TABLE_PA + " ( "
                + KEY_ID + INTEGER_PRIMARY_KEY_AUTOINCREMENT
                + KEY_AMOUNT + DECIMAL
                + KEY_DATE + INT8
                + KEY_TYPE + " TEXT"
                + " )";
        db.execSQL(CREATE_PA_TABLE);
    }

    //Create PA
    public void createPay(Pay pa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues curEntry = new ContentValues();

        curEntry.put(KEY_AMOUNT, pa.getAmount().doubleValue());
        curEntry.put(KEY_DATE, pa.getDate().getMillis());
        curEntry.put("type", pa.getType());

        db.insert(TABLE_PA, null, curEntry);
        db.close();
    }

    //Retrieve PA by ID
    public Pay retrievePayByID(int SQL_ID) {

        String query = SELECT_FROM + TABLE_PA + WHERE + KEY_ID + " = " + SQL_ID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Pay pa = new Pay();
        if (cursor.moveToFirst()) {
            pa.setId(Integer.parseInt(cursor.getString(0)));
            pa.setAmount(BigDecimal.valueOf(cursor.getDouble(1)));
            pa.setDate(new DateTime(cursor.getLong(2)));
            pa.setType(cursor.getString(3));
            return pa;
        } else {
            pa.setType(DB_READ_ERROR);
        }

        cursor.close();

        return pa;

    }

    //Retrieve PAs
    public List<Pay> retrieveAllPays() {
        List<Pay> pas = new ArrayList<>();

        String query = SELECT_FROM + TABLE_PA;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Pay pa;
        if (cursor.moveToFirst()) {
            do {
                pa = new Pay();
                pa.setId(Integer.parseInt(cursor.getString(0)));
                pa.setAmount(BigDecimal.valueOf(cursor.getDouble(1)));
                pa.setDate(new DateTime(cursor.getLong(2)));
                pa.setType(cursor.getString(3));
                pas.add(pa);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return pas;
    }

    //Update CO
    public int updatePay(Pay pa) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues curEntry = new ContentValues();

        curEntry.put(KEY_AMOUNT, pa.getAmount().doubleValue());
        curEntry.put(KEY_DATE, pa.getDate().getMillis());
        curEntry.put("type", pa.getType());

        int i = db.update(TABLE_PA, curEntry, format(ID_EQUALS_ANY, KEY_ID), new String[]{valueOf(pa.getId())});

        db.close();

        return i;

    }

    //Delete PA
    public void deletePay(int SQL_ID) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PA, format(ID_EQUALS_ANY, KEY_ID), new String[]{valueOf(SQL_ID)});

        db.close();
    }

    public void clearPays() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS PAY");
        onCreate(db);
    }

    //Payment Table

    //Create Payment Table
    private void createPMTable(SQLiteDatabase db) {
        String CREATE_PM_TABLE = CREATE_TABLE_IF_NOT_EXISTS + TABLE_PM + " ( "
                + KEY_ID + INTEGER_PRIMARY_KEY_AUTOINCREMENT
                + KEY_NAME + TEXT
                + KEY_AMOUNT + DECIMAL
                + KEY_DATE + INT8
                + KEY_CATEGORY + " TEXT"
                + " )";
        db.execSQL(CREATE_PM_TABLE);
    }

    //Create Payment
    public void createPayment(Payment pm) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues curEntry = new ContentValues();

        curEntry.put(KEY_NAME, pm.getName());
        curEntry.put(KEY_AMOUNT, pm.getAmount().doubleValue());
        curEntry.put(KEY_DATE, pm.getDate().getMillis());
        curEntry.put(KEY_CATEGORY, pm.getCategory());

        db.insert(TABLE_PM, null, curEntry);
        db.close();
    }

    //Retrieve PM by ID
    public Payment retrievePaymentByID(int SQL_ID) {

        String query = SELECT_FROM + TABLE_PM + WHERE + KEY_ID + " = " + SQL_ID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Payment pm = new Payment();
        if (cursor.moveToFirst()) {
            pm.setId(Integer.parseInt(cursor.getString(0)));
            pm.setName(cursor.getString(1));
            pm.setAmount(BigDecimal.valueOf(cursor.getDouble(2)));
            pm.setDate(new DateTime(cursor.getLong(3)));
            pm.setCategory(cursor.getString(4));
            return pm;
        } else {
            pm.setName(DB_READ_ERROR);
        }

        cursor.close();

        return pm;

    }

    //Retrieve PMs
    List<Payment> retrieveAllPayments() {
        List<Payment> pms = new ArrayList<>();

        String query = SELECT_FROM + TABLE_PM;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        Payment pm;
        if (cursor.moveToFirst()) {
            do {
                pm = new Payment();
                pm.setId(Integer.parseInt(cursor.getString(0)));
                pm.setName(cursor.getString(1));
                pm.setAmount(BigDecimal.valueOf(cursor.getDouble(2)));
                pm.setDate(new DateTime(cursor.getLong(3)));
                pm.setCategory(cursor.getString(4));
                pms.add(pm);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return pms;
    }

    //Update PM
    public int updatePayment(Payment pm) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues curEntry = new ContentValues();

        curEntry.put(KEY_NAME, pm.getName());
        curEntry.put(KEY_AMOUNT, pm.getAmount().doubleValue());
        curEntry.put(KEY_DATE, pm.getDate().getMillis());
        curEntry.put(KEY_CATEGORY, pm.getCategory());

        int i = db.update(TABLE_PM, curEntry, format(ID_EQUALS_ANY, KEY_ID), new String[]{valueOf(pm.getId())});

        db.close();

        return i;

    }

    //Delete PM
    public void deletePayment(int SQL_ID) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_PM, format(ID_EQUALS_ANY, KEY_ID), new String[]{valueOf(SQL_ID)});

        db.close();
    }

    public void clearPayments() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS Payment");
        onCreate(db);
    }

}