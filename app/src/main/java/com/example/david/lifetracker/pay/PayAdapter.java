package com.example.david.lifetracker.pay;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.david.lifetracker.R;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static java.lang.String.format;

public class PayAdapter extends ArrayAdapter<Pay> {

    public PayAdapter(Context context, int resource, int textViewResourceId, List<Pay> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View view = super.getView(position, convertView, parent);

        //Retrieving TextViews from the list item XML, then setting them to their corresponding values.

        TextView date = view.findViewById(R.id.payListDateTV);
        TextView amount = view.findViewById(R.id.payListAmountTV);

        Pay curPay = getItem(position);

        if (curPay != null) {
            DateTime dateTime = new DateTime(curPay.getDate());
            date.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));
            amount.setText(format(Locale.US, "$%s", curPay.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
        }

        return view;
    }
}