package com.example.david.lifetracker.cost;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.utility.SQLDB;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Locale;

import static java.lang.String.format;

public class CostActivity extends AppCompatActivity {

    Intent intent;
    int sqlId;
    private Spinner costTypeSPN;
    private Spinner costCategorySPN;
    private EditText costNameET;
    private EditText costAmountET;
    private EditText costSavedET;
    private TextView costDateTV;
    private Switch costPaidSWT;
    private DatePickerDialog.OnDateSetListener date;
    private Calendar myCalendar;
    private SQLDB sqldb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.cost_activity);

        viewInit();

        if (sqlId != -1) {
            buildFromDB();
        }
    }

    public void save(View v) {

        Cost cost = prepCost();

        if (sqlId == -1) {
            sqldb.createCost(cost);
        } else {
            sqldb.updateCost(cost);
        }

        finish();
    }

    public void delete(View v) {
        new AlertDialog.Builder(CostActivity.this)
                .setTitle("Delete Cost")
                .setMessage("Are you sure you want to delete this cost?")
                .setPositiveButton(android.R.string.yes, (dialog, which) -> delete())
                .setNegativeButton(android.R.string.no, (dialog, which) -> {
                    //No message needed
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void updateDateLabel() {
        DateTime dateTime = new DateTime(myCalendar.getTime());
        costDateTV.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));
    }

    private void buildFromDB() {
        try {
            Cost cost = sqldb.retrieveCostByID(sqlId);
            costNameET.setText(cost.getName());
            costAmountET.setText(format(Locale.US, "$%s", cost.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP)));
            costSavedET.setText(format(Locale.US, "$%s", cost.getSaved().setScale(2, BigDecimal.ROUND_HALF_UP)));
            myCalendar.setTimeInMillis(cost.getDate().getMillis());
            DateTime dateTime = new DateTime(myCalendar.getTime());
            costDateTV.setText((format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear())));

            String[] costTypes = getResources().getStringArray(R.array.cost_types);
            for (int i = 0; i < costTypes.length; i++) {
                if (cost.getType().equals(costTypes[i])) {
                    costTypeSPN.setSelection(i);
                    break;
                }
            }

            String[] categories = getResources().getStringArray(R.array.categories);
            for (int i = 0; i < categories.length; i++) {
                if (cost.getCategory().equals(categories[i])) {
                    costCategorySPN.setSelection(i);
                    break;
                }
            }

            costPaidSWT.setChecked(cost.isMonthPaid());

        } catch (Exception e) {
            Log.e("Cost Error", e.getMessage());
        }
    }

    private void delete() {
        if (sqlId == -1) {
            finish();
        } else {
            sqldb.deleteCost(sqlId);
            finish();
        }
    }

    private Cost prepCost() {
        Cost cost = new Cost();

        cost.setName(costNameET.getText().toString());
        if (!costAmountET.getText().toString().isEmpty()) {
            cost.setAmount(BigDecimal.valueOf(Double.parseDouble(costAmountET.getText().toString().replace("$", ""))));
        } else {
            cost.setAmount(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        if (!costSavedET.getText().toString().isEmpty()) {
            cost.setSaved(BigDecimal.valueOf(Double.parseDouble(costSavedET.getText().toString().replace("$", ""))));
        } else {
            cost.setSaved(BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP));
        }

        cost.setDate(new DateTime(myCalendar.getTimeInMillis()));
        cost.setCategory(costCategorySPN.getSelectedItem().toString());
        cost.setType(costTypeSPN.getSelectedItem().toString());
        cost.setIsMonthPaid(costPaidSWT.isChecked());
        cost.setId(sqlId);

        return cost;
    }


    private void viewInit() {
        sqldb = new SQLDB(getApplicationContext());

        costTypeSPN = findViewById(R.id.costTypeSPN);
        costCategorySPN = findViewById(R.id.costCategorySPN);

        costNameET = findViewById(R.id.costNameET);
        costAmountET = findViewById(R.id.costAmountET);
        costSavedET = findViewById(R.id.costSavedET);
        costDateTV = findViewById(R.id.costDateInputTV);
        costPaidSWT = findViewById(R.id.costPaidSWT);

        intent = getIntent();
        sqlId = intent.getIntExtra("SQL", -1);

        myCalendar = Calendar.getInstance();

        date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateLabel();
        };

        costDateTV.setOnClickListener(v -> new DatePickerDialog(CostActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this,
                R.array.cost_types, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        costTypeSPN.setAdapter(typeAdapter);

        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        costCategorySPN.setAdapter(categoryAdapter);
    }
}