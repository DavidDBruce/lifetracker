package com.example.david.lifetracker.costtrackeractivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.cost.Cost;
import com.example.david.lifetracker.pay.Pay;
import com.example.david.lifetracker.utility.BreakdownActivity;
import com.example.david.lifetracker.utility.ListActivity;
import com.example.david.lifetracker.utility.SQLDB;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.example.david.lifetracker.utility.Utilities.ANNUAL;
import static com.example.david.lifetracker.utility.Utilities.BI_WEEKLY;
import static com.example.david.lifetracker.utility.Utilities.CHECKING;
import static com.example.david.lifetracker.utility.Utilities.MONTHLY;
import static com.example.david.lifetracker.utility.Utilities.ONE_TIME;
import static com.example.david.lifetracker.utility.Utilities.SAVINGS;
import static com.example.david.lifetracker.utility.Utilities.TWO_DECIMAL_STRING;
import static com.example.david.lifetracker.utility.Utilities.TWO_DECIMAL_STRING_PAID;
import static com.example.david.lifetracker.utility.Utilities.TWO_DECIMAL_STRING_PUT_TO_SAVINGS;
import static com.example.david.lifetracker.utility.Utilities.addAmounts;
import static com.example.david.lifetracker.utility.Utilities.compareDatesToTheDay;
import static com.example.david.lifetracker.utility.Utilities.displayShortToast;
import static com.example.david.lifetracker.utility.Utilities.getNextPay;
import static com.example.david.lifetracker.utility.Utilities.getPayPeriods;
import static com.example.david.lifetracker.utility.Utilities.prepPayment;
import static java.lang.String.format;

@SuppressLint("InflateParams")
public class CostTrackerActivity extends AppCompatActivity {

    private static final Logger LOGGER = Logger.getLogger(CostTrackerActivity.class.getName());
    private TextView availableFundsNumTV;
    private TextView savingsNumTV;
    private TextView checkingNumTV;
    private List<Cost> costMainList;
    private CostMainAdapter costMainAdapter;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private SQLDB sqldb;

    private void init() {
        sqldb = new SQLDB(getApplicationContext());

        costMainList = new ArrayList<>();

        sharedPreferences = getPreferences(MODE_PRIVATE);

        TextView availableFundsTV = findViewById(R.id.availableFundsTV);
        availableFundsNumTV = findViewById(R.id.availableFundsNumTV);
        checkingNumTV = findViewById(R.id.checkingNumTV);
        savingsNumTV = findViewById(R.id.savingsNumTV);

        editor = sharedPreferences.edit();

        if (sharedPreferences.contains(CHECKING)) {
            checkingNumTV.setText(sharedPreferences.getString(CHECKING, "0"));
        } else {
            editor.putString(CHECKING, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            editor.apply();
        }

        if (sharedPreferences.contains(SAVINGS)) {
            savingsNumTV.setText(sharedPreferences.getString(SAVINGS, "0"));
        } else {
            editor.putString(SAVINGS, BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            editor.apply();
        }

        checkingNumTV.setOnClickListener(getCheckingOnClickListener());

        savingsNumTV.setOnClickListener(getSavingsOnClickListener());

        availableFundsTV.setOnClickListener(v -> populateAvailable());

        try {
            if (!sqldb.retrieveAllPays().isEmpty()) {
                checkPayPeriod();
            }
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cost_tracker_main);
        init();
        populateAvailable();
        checkIssues();
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLV();
        populateAvailable();
    }

    /**
     * Opens the list activity with the cost formatting.
     *
     * @param v View required for onClick to register from XML
     */

    public void costs(View v) {
        openList("Costs");
    }

    /**
     * Opens the list activity with the pay formatting.
     *
     * @param v View required for onClick to register from XML
     */

    public void paychecks(View v) {
        openList("Paychecks");
    }

    /**
     * Opens the list activity with the payment formatting.
     *
     * @param v View required for onClick to register from XML
     */

    public void payments(View v) {
        openList("Payments");
    }

    /**
     * Opens the Breakdown page.
     *
     * @param v View required for onClick to register from XML
     */

    public void breakdown(View v) {
        Intent intent = new Intent(this, BreakdownActivity.class);
        startActivity(intent);
    }

    /**
     * Opens the list using the input string.
     *
     * @param list String containing type of list.
     */

    protected void openList(String list) {
        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra("listType", list);
        startActivity(intent);
    }

    /**
     * Refreshes the TextViews of calculated numbers for availableFunds, checking, and savings.
     */

    @SuppressLint("SetTextI18n")
    protected void populateAvailable() {
        BigDecimal checking = BigDecimal.valueOf(Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(CHECKING, "0"))));
        BigDecimal savings = BigDecimal.valueOf(Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(SAVINGS, "0"))));
        checkingNumTV.setText(checking.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        savingsNumTV.setText(savings.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        try {
            availableFundsNumTV.setText(getAvailableFunds(checking, savings).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            buildLV();
        } catch (Exception e) {
            availableFundsNumTV.setText(format(Locale.US, "%s", "Error"));
        }
    }

    protected BigDecimal getAvailableFunds(BigDecimal checking, BigDecimal savings) {
        PayPeriodCost payPeriodCost = calculatePayPeriodCost();
        if (savings.subtract(payPeriodCost.savingsCost).subtract(totalSaved()).compareTo(BigDecimal.ZERO) < 0) {
            return checking.subtract(payPeriodCost.checkingCost).add(savings.subtract(payPeriodCost.savingsCost.subtract(totalSaved())));
        } else {
            return checking.subtract(payPeriodCost.checkingCost);
        }
    }

    protected BigDecimal totalSaved() {
        try {
            return addAmounts(sqldb.retrieveAllCosts().stream().map(Cost::getSaved).collect(Collectors.toCollection(ArrayList::new)));
        } catch (Exception e) {
            displayShortToast(this, "DB Error");
            return BigDecimal.ZERO;
        }
    }

    /**
     * Populates the main ListView with current costs and future non-monthly costs that one might want to save up for.
     */

    protected void buildLV() {
        costMainAdapter = new CostMainAdapter(getApplicationContext(), R.layout.cost_main_list_item, R.id.costMainNameTV, costMainList);
        ListView mainLV = findViewById(R.id.mainLV);
        mainLV.setAdapter(costMainAdapter);
        mainLV.setOnItemClickListener(getMainLVOnItemClickListener());
    }

    protected void checkIssues() {
        final BigDecimal savingsAmt = BigDecimal.valueOf(Double.parseDouble(savingsNumTV.getText().toString().replace("$", "")));
        final BigDecimal savingsStored = getSavingsStored();
        if (savingsStored.compareTo(BigDecimal.ZERO) < 0) {
            return;
        }

        if (savingsStored.compareTo(savingsAmt) != 0) {
            TextView savingsTV = findViewById(R.id.savingsTV);
            if (savingsStored.compareTo(savingsAmt) > 0) {
                savingsTV.setBackgroundColor(Color.parseColor("#ff6666"));
                savingsTV.setOnClickListener(getSavingsInRedOnClickListener(savingsAmt, savingsStored));
            } else {
                savingsTV.setBackgroundColor(Color.parseColor("#66ff66"));
                savingsTV.setOnClickListener(getSavingsSurplusOnClickListener(savingsAmt, savingsStored));
            }
        }
    }

    protected BigDecimal getSavingsStored() {
        try {
            return addAmounts(sqldb.retrieveAllCosts().stream().map(Cost::getSaved).collect(Collectors.toCollection(ArrayList::new)));
        } catch (Exception e) {
            return BigDecimal.valueOf(-100, 2);
        }
    }

    /**
     * Calculates how much is to be removed from checking for current pay period costs.
     *
     * @return A PayPeriodCost which contains the calculated savings and checking amounts.
     */

    protected PayPeriodCost calculatePayPeriodCost() {
        List<Cost> costList = sqldb.retrieveAllCosts();
        costMainList = new ArrayList<>();

        Collections.sort(costList);

        PayPeriodCost payPeriodCost = new PayPeriodCost();
        payPeriodCost.checkingCost = BigDecimal.ZERO;
        payPeriodCost.savingsCost = BigDecimal.ZERO;
        DateTime payDate = getNextPay(sqldb.retrieveAllPays());

        if (payDate.getMillis() == 0) {
            return payPeriodCost;
        }

        DateTime curTime = DateTime.now();
        costList.stream()
                .filter(cost -> !cost.isMonthPaid())
                .forEach(cost -> handleCost(cost, curTime, payDate, payPeriodCost));
        return payPeriodCost;
    }

    private void handleCost(Cost cost, DateTime curTime, DateTime payDate, PayPeriodCost payPeriodCost) {
        DateTime costDate = cost.getDate();
        if ((compareDatesToTheDay(curTime, costDate) <= 0 && compareDatesToTheDay(costDate, payDate) <= 0) || compareDatesToTheDay(costDate, curTime) < 0) {
            payPeriodCost.checkingCost = payPeriodCost.checkingCost.add(cost.getAmount().subtract(cost.getSaved()));
            costMainList.add(cost);
        } else {
            if (ONE_TIME.equals(cost.getType())) {
                payPeriodCost.savingsCost = payPeriodCost.savingsCost.add(cost.getAmount().subtract(cost.getSaved()).divide(getPayPeriods(costDate, payDate), BigDecimal.ROUND_HALF_UP));
                costMainList.add(cost);
            } else if (ANNUAL.equals(cost.getType())) {
                payPeriodCost.savingsCost = payPeriodCost.savingsCost.add(cost.getAmount().divide(getPayPeriods(costDate, payDate), BigDecimal.ROUND_HALF_UP));
                costMainList.add(cost);
            }
        }
    }

    protected View.OnClickListener getCheckingAddOnClickListener(final View view) {
        return v -> {
            double userInput = Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString());
            double currentChecking = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(CHECKING, "0")));
            editor.putString(CHECKING, Double.toString(userInput + currentChecking));
            editor.apply();
            populateAvailable();
        };
    }

    protected View.OnClickListener getCheckingRemoveOnClickListener(final View view) {
        return v -> {
            double userInput = Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString());
            double currentChecking = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(CHECKING, "0")));
            editor.putString(CHECKING, Double.toString(currentChecking - userInput));
            editor.apply();
            populateAvailable();
        };
    }

    protected View.OnClickListener getCheckingTransferOnClickListener(final View view) {
        return v -> new AlertDialog.Builder(CostTrackerActivity.this)
                .setTitle("Are you sure?")
                .setMessage(format("This will move %s to your savings.", BigDecimal.valueOf(Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString())).setScale(2, BigDecimal.ROUND_HALF_UP).toString()))
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    double userInput = Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString());
                    double currentChecking = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(CHECKING, "0")));
                    double currentSavings = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(SAVINGS, "0")));
                    editor.putString(CHECKING, Double.toString(currentChecking - userInput));
                    editor.putString(SAVINGS, Double.toString(userInput + currentSavings));
                    editor.apply();
                    populateAvailable();
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> populateAvailable())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    protected View.OnClickListener getCheckingOnClickListener() {
        return v -> {

            final View view = getLayoutInflater().inflate(R.layout.transfer_dialog, null);
            new AlertDialog.Builder(CostTrackerActivity.this)
                    .setNegativeButton("Close", (dialog, which) -> {
                        //No Message Needed
                    })
                    .setView(view)
                    .setOnDismissListener(dialog -> {
                        //No Message Needed
                    })
                    .show();

            TextView dialogTitleTV = view.findViewById(R.id.dialogTitleTV);
            dialogTitleTV.setText(format(Locale.US, "%s", "Checking"));

            Button add = view.findViewById(R.id.dialogAddBTN);
            Button remove = view.findViewById(R.id.dialogRemoveBTN);
            Button transfer = view.findViewById(R.id.dialogTransferBTN);

            add.setOnClickListener(getCheckingAddOnClickListener(view));
            remove.setOnClickListener(getCheckingRemoveOnClickListener(view));
            transfer.setOnClickListener(getCheckingTransferOnClickListener(view));
        };
    }

    protected View.OnClickListener getSavingsAddOnClickListener(final View view) {
        return v -> {
            double userInput = Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString());
            double currentSavings = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(SAVINGS, "0")));
            editor.putString(SAVINGS, Double.toString(userInput + currentSavings));
            editor.apply();
            populateAvailable();
        };
    }

    protected View.OnClickListener getSavingsRemoveOnClickListener(final View view) {
        return v -> {
            double userInput = Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString());
            double currentSavings = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(SAVINGS, "0")));
            editor.putString(SAVINGS, Double.toString(currentSavings - userInput));
            editor.apply();
            populateAvailable();
        };
    }

    protected View.OnClickListener getSavingsTransferOnClickListener(final View view) {
        return v -> new AlertDialog.Builder(CostTrackerActivity.this)
                .setTitle("Are you sure?")
                .setMessage(format("This will move %s to your checking.", BigDecimal.valueOf(Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString())).setScale(2, BigDecimal.ROUND_HALF_UP).toString()))
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    double userInput = Double.parseDouble(((EditText) view.findViewById(R.id.dialogInputET)).getText().toString());
                    double currentChecking = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(CHECKING, "0")));
                    double currentSavings = Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(SAVINGS, "0")));
                    editor.putString(SAVINGS, Double.toString(currentSavings - userInput));
                    editor.putString(CHECKING, Double.toString(userInput + currentChecking));
                    editor.apply();
                    populateAvailable();
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> populateAvailable())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    protected View.OnClickListener getSavingsOnClickListener() {
        return v -> {
            LayoutInflater inflater = getLayoutInflater();
            final View view = inflater.inflate(R.layout.transfer_dialog, null);
            final AlertDialog.Builder alert = new AlertDialog.Builder(CostTrackerActivity.this);
            alert.setNegativeButton("Close", (dialog, which) -> {
                //No message needed.
            });
            alert.setView(view);
            alert.setOnDismissListener(dialog -> {
                //No message needed.
            });
            alert.show();

            TextView dialogTitleTV = view.findViewById(R.id.dialogTitleTV);
            dialogTitleTV.setText(format(Locale.US, "%s", "Savings"));

            Button add = view.findViewById(R.id.dialogAddBTN);
            Button remove = view.findViewById(R.id.dialogRemoveBTN);
            Button transfer = view.findViewById(R.id.dialogTransferBTN);

            add.setOnClickListener(getSavingsAddOnClickListener(view));
            remove.setOnClickListener(getSavingsRemoveOnClickListener(view));
            transfer.setOnClickListener(getSavingsTransferOnClickListener(view));
        };
    }

    protected View.OnClickListener getSavingsInRedOnClickListener(BigDecimal savingsAmt, BigDecimal savingsStored) {
        return v -> new AlertDialog.Builder(CostTrackerActivity.this)
                .setTitle("Savings Mismatch")
                .setMessage(format("Your savings are $%s but are listed in costs as $%s.", savingsAmt.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), savingsStored.setScale(2, BigDecimal.ROUND_HALF_UP).toString()))
                .show();
    }

    protected View.OnClickListener getSavingsSurplusOnClickListener(BigDecimal savingsAmt, BigDecimal savingsStored) {
        return v -> new AlertDialog.Builder(CostTrackerActivity.this)
                .setTitle("Savings Surplus")
                .setMessage(format("Your savings are $%s but are listed in costs as $%s.", savingsAmt.setScale(2, BigDecimal.ROUND_HALF_UP).toString(), savingsStored.setScale(2, BigDecimal.ROUND_HALF_UP).toString()))
                .show();
    }

    protected AdapterView.OnItemClickListener getMainLVOnItemClickListener() {
        return (parent, view, position, id) -> new AlertDialog.Builder(CostTrackerActivity.this)
                .setTitle("Make Payment")
                .setMessage("This will move to your payments and update if the cost is recurring.")
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    Cost cost = (Cost) parent.getItemAtPosition(position);
                    BigDecimal curChecking = BigDecimal.valueOf(Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(CHECKING, "0.0"))));
                    BigDecimal curSavings = BigDecimal.valueOf(Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(SAVINGS, "0.0"))));

                    try {
                        determineAndPerformCost(cost, curChecking, curSavings);
                    } catch (Exception e) {
                        LOGGER.info(e.getMessage());
                    }
                    costMainAdapter.remove(cost);
                    costMainAdapter.notifyDataSetChanged();
                    populateAvailable();
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> {
                    //No message needed.
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void determineAndPerformCost(Cost cost, BigDecimal curChecking, BigDecimal curSavings) {
        if (ONE_TIME.equals(cost.getType())) {
            if (isInPayPeriod(cost) || isCostOverdue(cost)) {
                performOneTimeCostInPayPeriod(cost, curChecking);
                displayShortToast(getApplicationContext(), format(Locale.US, TWO_DECIMAL_STRING_PAID, cost.getAmount().subtract(cost.getSaved()).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
            } else {
                BigDecimal putToSavings = cost.getAmount().add(cost.getSaved()).divide(getPayPeriods(new DateTime(cost.getDate()), getNextPay(sqldb.retrieveAllPays())), BigDecimal.ROUND_HALF_UP);
                handleUpdatingCostIfNotInPayPeriod(cost, curChecking, curSavings, putToSavings);
                displayShortToast(getApplicationContext(), format(Locale.US, TWO_DECIMAL_STRING_PUT_TO_SAVINGS, putToSavings.setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
            }
        } else if (BI_WEEKLY.equals(cost.getType())) {
            performBiWeeklyCost(cost, curChecking);
            displayShortToast(getApplicationContext(), format(Locale.US, TWO_DECIMAL_STRING_PAID, cost.getAmount().subtract(cost.getSaved()).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
        } else if (MONTHLY.equals(cost.getType())) {
            performMonthlyCost(cost, curChecking);
            displayShortToast(getApplicationContext(), format(Locale.US, TWO_DECIMAL_STRING_PAID, cost.getAmount().subtract(cost.getSaved()).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
        } else if (ANNUAL.equals(cost.getType())) {
            if (isInPayPeriod(cost)) {
                performAnnualCostInPayPeriod(cost, curChecking);
                displayShortToast(getApplicationContext(), format(Locale.US, TWO_DECIMAL_STRING_PAID, cost.getAmount().subtract(cost.getSaved()).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
            } else {
                BigDecimal putToSavings = cost.getAmount().subtract(cost.getSaved()).divide(getPayPeriods(new DateTime(cost.getDate()), getNextPay(sqldb.retrieveAllPays())), BigDecimal.ROUND_HALF_UP);
                handleUpdatingCostIfNotInPayPeriod(cost, curChecking, curSavings, putToSavings);
                displayShortToast(getApplicationContext(), format(Locale.US, TWO_DECIMAL_STRING_PUT_TO_SAVINGS, putToSavings.setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
            }
        }
    }

    protected boolean isCostOverdue(Cost cost) {
        return compareDatesToTheDay(new DateTime(cost.getDate()), DateTime.now()) == -1;
    }


    protected void performAnnualCostInPayPeriod(Cost cost, BigDecimal curChecking) {
        sqldb.createPayment(prepPayment(cost, getPayPeriods(new DateTime(cost.getDate()), getNextPay(sqldb.retrieveAllPays()))));

        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(CHECKING, curChecking.subtract(cost.getAmount().subtract(cost.getSaved())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        sharedPreferencesEditor.apply();

        cost.setDate(new DateTime(cost.getDate()).plusYears(1));
        cost.setSaved(BigDecimal.ZERO);
        cost.setIsMonthPaid(true);

        sqldb.updateCost(cost);
    }

    private void performMonthlyCost(Cost cost, BigDecimal curChecking) {
        sqldb.createPayment(prepPayment(cost, getPayPeriods(new DateTime(cost.getDate()), getNextPay(sqldb.retrieveAllPays()))));

        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(CHECKING, curChecking.subtract(cost.getAmount().subtract(cost.getSaved())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        sharedPreferencesEditor.apply();

        cost.setDate(new DateTime(cost.getDate()).plusMonths(1));
        cost.setSaved(BigDecimal.ZERO);
        cost.setIsMonthPaid(true);

        sqldb.updateCost(cost);
    }

    private void performBiWeeklyCost(Cost cost, BigDecimal curChecking) {
        sqldb.createPayment(prepPayment(cost, getPayPeriods(new DateTime(cost.getDate()), getNextPay(sqldb.retrieveAllPays()))));

        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(CHECKING, curChecking.subtract(cost.getAmount().subtract(cost.getSaved())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        sharedPreferencesEditor.apply();

        cost.setDate(new DateTime(cost.getDate()).plusDays(14));
        cost.setSaved(BigDecimal.ZERO);
        cost.setIsMonthPaid(true);

        sqldb.updateCost(cost);
    }

    private void handleUpdatingCostIfNotInPayPeriod(Cost cost, BigDecimal curChecking, BigDecimal curSavings, BigDecimal putToSavings) {
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(SAVINGS, TWO_DECIMAL_STRING.format(putToSavings.add(curSavings).setScale(2, BigDecimal.ROUND_HALF_UP).toString()));

        sharedPreferencesEditor.putString(CHECKING, TWO_DECIMAL_STRING.format(curChecking.subtract(putToSavings)));
        sharedPreferencesEditor.apply();

        cost.setSaved(cost.getSaved().add(putToSavings));
        cost.setIsMonthPaid(true);

        sqldb.updateCost(cost);
    }

    private void performOneTimeCostInPayPeriod(Cost cost, BigDecimal curChecking) {
        sqldb.createPayment(prepPayment(cost, getPayPeriods(new DateTime(cost.getDate()), getNextPay(sqldb.retrieveAllPays()))));

        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(CHECKING, curChecking.subtract(cost.getAmount().subtract(cost.getSaved())).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
        sharedPreferencesEditor.apply();

        sqldb.deleteCost(cost.getId());
    }

    protected void checkPayPeriod() {
        DateTime rightNow = DateTime.now();
        List<Pay> payList;
        List<Cost> costList;
        try {
            payList = sqldb.retrieveAllPays();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            payList = new ArrayList<>();
        }

        Pay pay = payList.get(0);

        if (compareDatesToTheDay(new DateTime(pay.getDate()), rightNow) == -1) {
            if (pay.getType().equals(BI_WEEKLY)) {
                pay.setDate(pay.getDate().plusDays(14));
                SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                BigDecimal curChecking = BigDecimal.valueOf(Double.parseDouble(Objects.requireNonNull(sharedPreferences.getString(CHECKING, "0"))));
                sharedPreferencesEditor.putString(CHECKING, TWO_DECIMAL_STRING.format(curChecking.add(pay.getAmount())));
                sharedPreferencesEditor.apply();
                sqldb.updatePay(pay);
            }

            try {
                costList = sqldb.retrieveAllCosts();
            } catch (Exception e) {
                costList = new ArrayList<>();
                LOGGER.info(e.getMessage());
            }

            for (Cost cost : costList) {
                cost.setIsMonthPaid(false);
                sqldb.updateCost(cost);
            }
        }
    }

    protected boolean isInPayPeriod(Cost cost) {
        try {
            DateTime nextPay = getNextPay(sqldb.retrieveAllPays());
            return compareDatesToTheDay(cost.getDate(), nextPay) < 0 && (compareDatesToTheDay(cost.getDate(), nextPay.minusDays(14)) < 0);
        } catch (Exception e) {
            displayShortToast(this, "DB Error in isInPayPeriod");
            return false;
        }
    }
}
