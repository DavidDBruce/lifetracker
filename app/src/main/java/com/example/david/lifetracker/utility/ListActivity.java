package com.example.david.lifetracker.utility;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.cost.Cost;
import com.example.david.lifetracker.cost.CostActivity;
import com.example.david.lifetracker.cost.CostAdapter;
import com.example.david.lifetracker.pay.PayActivity;
import com.example.david.lifetracker.pay.PayAdapter;
import com.example.david.lifetracker.payment.Payment;
import com.example.david.lifetracker.payment.PaymentActivity;
import com.example.david.lifetracker.payment.PaymentAdapter;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.example.david.lifetracker.utility.Utilities.COULDNT_LOAD_LIST_OF;
import static com.example.david.lifetracker.utility.Utilities.displayShortToast;
import static java.lang.String.format;

public class ListActivity extends AppCompatActivity {

    CostAdapter costAdapter;
    PayAdapter payAdapter;
    PaymentAdapter paymentAdapter;

    TextView listTitleTV;

    ListView costLV;
    ListView payLV;
    ListView paymentLV;

    String listType;

    Intent inputIntent;

    SQLDB sqldb;

    FloatingActionButton listAddFAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.generic_list);

        sqldb = new SQLDB(getApplicationContext());

        listTitleTV = findViewById(R.id.listTitleTV);

        inputIntent = getIntent();
        listType = inputIntent.getStringExtra("listType");

        listAddFAB = findViewById(R.id.listAddFAB);

        listTitleTV.setText(listType);

        buildLV();

    }

    @Override
    protected void onResume() {
        super.onResume();
        buildLV();
    }

    private void buildLV() {
        if ("Costs".equals(listType)) {
            try {
                buildCosts();
            } catch (Exception e) {
                displayShortToast(this, format("%s%s", COULDNT_LOAD_LIST_OF, listType));
            }

        } else if ("Paychecks".equals(listType)) {
            try {
                buildPaychecks();
            } catch (Exception e) {
                displayShortToast(this, format("%s%s", COULDNT_LOAD_LIST_OF, listType));
            }

        } else if ("Payments".equals(listType)) {
            try {
                buildPayments();
            } catch (Exception e) {
                displayShortToast(this, format("%s%s", COULDNT_LOAD_LIST_OF, listType));
            }

        }
    }


    private void buildCosts() {
        List<Cost> costList = sqldb.retrieveAllCosts();
        Collections.sort(costList);

        costAdapter = new CostAdapter(getApplicationContext(), R.layout.cost_list_item, R.id.costListNameTV, costList);
        costLV = findViewById(R.id.listLV);
        costLV.setAdapter(costAdapter);

        costLV.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(getApplicationContext(), CostActivity.class);
            intent.putExtra("SQL", Objects.requireNonNull(costAdapter.getItem(position)).getId());
            startActivity(intent);
        });
    }

    private void buildPaychecks() {
        payAdapter = new PayAdapter(getApplicationContext(), R.layout.pay_list_item, R.id.payListAmountTV, sqldb.retrieveAllPays());
        payLV = findViewById(R.id.listLV);
        payLV.setAdapter(payAdapter);

        payLV.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(getApplicationContext(), PayActivity.class);
            intent.putExtra("SQL", Objects.requireNonNull(payAdapter.getItem(position)).getId());
            startActivity(intent);
        });
    }

    private void buildPayments() {
        List<Payment> paymentList = sqldb.retrieveAllPayments();
        Collections.sort(paymentList);

        paymentAdapter = new PaymentAdapter(getApplicationContext(), R.layout.payment_list_item, R.id.paymentListNameTV, paymentList);
        paymentLV = findViewById(R.id.listLV);
        paymentLV.setAdapter(paymentAdapter);
        paymentLV.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
            intent.putExtra("SQL", Objects.requireNonNull(paymentAdapter.getItem(position)).getId());
            startActivity(intent);
        });
    }

    public void openList(View v) {
        if ("Costs".equals(listType)) {
            Intent intent = new Intent(getApplicationContext(), CostActivity.class);
            startActivity(intent);
        } else if ("Paychecks".equals(listType)) {
            Intent intent = new Intent(getApplicationContext(), PayActivity.class);
            startActivity(intent);
        } else if ("Payments".equals(listType)) {
            Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
            startActivity(intent);
        }
    }
}