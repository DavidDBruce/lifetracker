package com.example.david.lifetracker.utility;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.costtrackeractivity.CostTrackerActivity;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
    }

    public void openCostTracker(View v) {
        Intent intent = new Intent(this, CostTrackerActivity.class);
        startActivity(intent);
    }

    public void openDeveloperOptions(View v) {
        Intent intent = new Intent(this, DeveloperOptions.class);
        startActivity(intent);
    }
}