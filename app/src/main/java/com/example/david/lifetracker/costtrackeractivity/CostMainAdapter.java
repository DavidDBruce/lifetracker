package com.example.david.lifetracker.costtrackeractivity;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.cost.Cost;
import com.example.david.lifetracker.pay.Pay;
import com.example.david.lifetracker.utility.SQLDB;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static com.example.david.lifetracker.utility.Utilities.TWO_DECIMAL_STRING;
import static com.example.david.lifetracker.utility.Utilities.getNextPay;
import static com.example.david.lifetracker.utility.Utilities.getPayPeriods;
import static java.lang.String.format;

class CostMainAdapter extends ArrayAdapter<Cost> {

    CostMainAdapter(Context context, int resource, int textViewResourceId, List<Cost> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View view = super.getView(position, convertView, parent);
        List<Pay> payList;
        long payDate;

        TextView name = view.findViewById(R.id.costMainNameTV);
        TextView amount = view.findViewById(R.id.costMainAmountTV);
        TextView date = view.findViewById(R.id.costMainDateTV);

        Cost curCost = getItem(position);
        DateTime curCostDate;

        try {
            payList = new SQLDB(parent.getContext()).retrieveAllPays();
            payDate = getNextPay(payList).getMillis();
        } catch (Exception e) {
            payDate = 0L;
        }

        if (curCost != null) {
            curCostDate = new DateTime(curCost.getDate());
            name.setText(curCost.getName());
            date.setText(format(Locale.US, "%d/%d/%d", curCostDate.getMonthOfYear(), curCostDate.getDayOfMonth(), curCostDate.getYear()));
            if (DateTime.now().getMillis() > curCostDate.getMillis()) {
                amount.setText(TWO_DECIMAL_STRING.format(curCost.getAmount()));
                view.setBackgroundColor(Color.parseColor("#ff6666"));
            } else if (curCostDate.getMillis() < payDate) {
                amount.setText(TWO_DECIMAL_STRING.format(curCost.getAmount()));
                view.setBackgroundColor(Color.parseColor("#ffff66"));
            } else {
                view.setBackgroundColor(Color.parseColor("#66ff66"));
                amount.setText(TWO_DECIMAL_STRING.format(
                        curCost.getAmount().subtract(curCost.getSaved().divide(
                                getPayPeriods(
                                        new DateTime(curCostDate),
                                        new DateTime(payDate)),
                                BigDecimal.ROUND_HALF_UP))));
            }
        }

        return view;
    }
}