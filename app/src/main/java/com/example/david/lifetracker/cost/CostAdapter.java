package com.example.david.lifetracker.cost;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.david.lifetracker.R;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static java.lang.String.format;

public class CostAdapter extends ArrayAdapter<Cost> {

    public CostAdapter(Context context, int resource, int textViewResourceId, List<Cost> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View view = super.getView(position, convertView, parent);

        //Retrieving TextViews from the list item XML, then setting them to their corresponding values.

        TextView name = view.findViewById(R.id.costListNameTV);
        TextView amount = view.findViewById(R.id.costListAmountTV);
        TextView type = view.findViewById(R.id.costListTypeTV);
        TextView category = view.findViewById(R.id.costListCategoryTV);
        TextView date = view.findViewById(R.id.costListDateTV);

        Cost curCost = getItem(position);
        DateTime dateTime;

        if (curCost != null) {

            dateTime = new DateTime(curCost.getDate());
            name.setText(curCost.getName());
            amount.setText(format(Locale.US, "$%s", curCost.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
            type.setText(curCost.getType());
            category.setText(curCost.getCategory());
            date.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));
        } else {
            dateTime = DateTime.now();
        }

        if (DateTime.now().getMillis() > dateTime.getMillis()) {
            view.setBackgroundColor(Color.parseColor("#ff6666"));
        }

        return view;
    }
}