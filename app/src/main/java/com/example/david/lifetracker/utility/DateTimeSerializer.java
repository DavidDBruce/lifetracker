package com.example.david.lifetracker.utility;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.lang.reflect.Type;

public class DateTimeSerializer implements JsonSerializer {
    @Override
    public JsonElement serialize(Object json, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(ISODateTimeFormat.dateTime().print((DateTime) json));
    }
}