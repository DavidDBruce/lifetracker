package com.example.david.lifetracker.payment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.david.lifetracker.R;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static java.lang.String.format;

public class PaymentAdapter extends ArrayAdapter<Payment> {

    public PaymentAdapter(Context context, int resource, int textViewResourceId, List<Payment> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View view = super.getView(position, convertView, parent);

        //Retrieving TextViews from the list item XML, then setting them to their corresponding values.

        TextView name = view.findViewById(R.id.paymentListNameTV);
        TextView amount = view.findViewById(R.id.paymentListAmountTV);
        TextView date = view.findViewById(R.id.paymentListDateTV);
        TextView category = view.findViewById(R.id.paymentListCategoryTV);

        Payment curPayment = getItem(position);

        if (curPayment != null) {
            name.setText(curPayment.getName());
            amount.setText(format(Locale.US, "$%s", curPayment.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toString()));
            DateTime dateTime = new DateTime(curPayment.getDate());
            date.setText(format(Locale.US, "%d/%d/%d", dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getYear()));
            category.setText(curPayment.getCategory());
        }

        return view;
    }
}