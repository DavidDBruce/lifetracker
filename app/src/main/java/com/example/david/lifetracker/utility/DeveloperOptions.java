package com.example.david.lifetracker.utility;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.david.lifetracker.R;
import com.example.david.lifetracker.cost.Cost;
import com.example.david.lifetracker.payment.Payment;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;

import static com.example.david.lifetracker.utility.Utilities.GSON;

public class DeveloperOptions extends AppCompatActivity {

    private static final String TAG = "FILE CREATE TAG";
    private static final Integer JSON_COSTS_RESULT = 86;
    private static final Integer JSON_PAYMENTS_REQUEST = 87;

    private SQLDB sqldb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.developer_options_activity);
        sqldb = new SQLDB(getApplicationContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        Uri uri;
        if (resultCode == Activity.RESULT_OK && resultData != null) {
            uri = resultData.getData();
        } else {
            Log.e(TAG, "Not a populated file.");
            throw new RuntimeException();
        }

        if (requestCode == JSON_COSTS_RESULT) {
            restoreCosts(uri);
        } else if (requestCode == JSON_PAYMENTS_REQUEST) {
            restorePayments(uri);
        }
    }

    public void backupCosts(View v) {
        BackupUtil backupUtil = new BackupUtil(getApplicationContext());
        try {
            Intent emailIntent = backupUtil.createIntentToBackupCostsPureText();
            sendEmail(emailIntent);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to backup.");
        }
    }

    public void backupPayments(View v) {
        BackupUtil backupUtil = new BackupUtil(getApplicationContext());
        try {
            Intent emailIntent = backupUtil.createIntentToBackupPaymentsPureText();
            sendEmail(emailIntent);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to backup.");
        }
    }

    public void getCostsRestoreUri(View v) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, JSON_COSTS_RESULT);
    }

    public void getPaymentsRestoreUri(View v) {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, JSON_PAYMENTS_REQUEST);
    }

    private void restoreCosts(Uri uri) {
        String costsJson = readTextFromUri(uri);
        List<Cost> costs = GSON.fromJson(costsJson, new TypeToken<List<Cost>>() {
        }.getType());
        costs.forEach(cost -> sqldb.createCost(cost));
    }

    private void restorePayments(Uri uri) {
        String paymentsJson = readTextFromUri(uri);
        List<Payment> payments = GSON.fromJson(paymentsJson, new TypeToken<List<Payment>>() {
        }.getType());
        payments.forEach(payment -> sqldb.createPayment(payment));
    }

    private String readTextFromUri(Uri uri) {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream =
                     getContentResolver().openInputStream(uri);
             BufferedReader reader = new BufferedReader(
                     new InputStreamReader(Objects.requireNonNull(inputStream)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException ioe) {
            Log.e(TAG, "Failed to parse file.");
            throw new RuntimeException();
        }
        return stringBuilder.toString();
    }

    private void sendEmail(Intent emailIntent) {
        startActivity(emailIntent);
    }
}
