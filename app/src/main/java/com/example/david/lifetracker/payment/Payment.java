package com.example.david.lifetracker.payment;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Created by DDB-0 on 9/21/2017.
 */

public class Payment implements Comparator<Payment>, Comparable<Payment> {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("amount")
    private BigDecimal amount;
    @SerializedName("date")
    private DateTime date;
    @SerializedName("category")
    private String category;

    public Payment() {

        id = -1;
        name = "";
        amount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
        date = DateTime.now();
        category = "";

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int compareTo(@NonNull Payment pmt) {
        return Long.compare(pmt.getDate().getMillis(), date.getMillis());
    }

    @Override
    public int compare(Payment leftPayment, Payment rightPayment) {

        if (leftPayment.getDate().getMillis() < rightPayment.getDate().getMillis()) {
            return 1;
        } else if (leftPayment.getDate() == rightPayment.getDate()) {
            return 0;
        } else {
            return -1;
        }


    }
}