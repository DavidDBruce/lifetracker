package com.example.david.lifetracker.utility;

import android.content.Context;
import android.widget.Toast;

import com.example.david.lifetracker.cost.Cost;
import com.example.david.lifetracker.pay.Pay;
import com.example.david.lifetracker.payment.Payment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Utilities {

    static final String DATE_FORMAT = "%d/%d/%d";
    public static final String TWO_DECIMAL_STRING_PAID = "$%s paid.";
    public static final String CHECKING = "checking";
    public static final String SAVINGS = "savings";
    public static final DecimalFormat TWO_DECIMAL_STRING;

    static {
        TWO_DECIMAL_STRING = new DecimalFormat();
        TWO_DECIMAL_STRING.setMaximumFractionDigits(2);
        TWO_DECIMAL_STRING.setMinimumFractionDigits(2);
    }

    public static final String BI_WEEKLY = "Bi-Weekly";
    public static final String TWO_DECIMAL_STRING_PUT_TO_SAVINGS = "$%s put to savings.";
    public static final String MONTHLY = "Monthly";
    public static final String ANNUAL = "Annual";
    public static final String ONE_TIME = "One Time";
    static final String COULDNT_LOAD_LIST_OF = "Couldn't load list of ";

    public static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(DateTime.class, new DateTimeSerializer())
            .registerTypeAdapter(DateTime.class, new DateTimeDeserializer())
            .create();

    private Utilities() {
        //Not to be instantiated.
    }

    /**
     * returns -1 if date 1 is before date 2, 0 if same day, 1 if date 1 is after date 2.
     *
     * @param dateTimeA first date to compare.
     * @param dateTimeB second date to compare.
     * @return -1 if date A is before date B, 0 if same day, 1 if date 1 is after date 2.
     */

    public static int compareDatesToTheDay(DateTime dateTimeA, DateTime dateTimeB) {
        if (dateTimeA.getYear() < dateTimeB.getYear()) {
            return -1;
        } else if (dateTimeA.getYear() > dateTimeB.getYear()) {
            return 1;
        } else {
            return Integer.compare(dateTimeA.getDayOfYear(), dateTimeB.getDayOfYear());
        }
    }

    /**
     * Returns the number of payPeriods before the current cost is due.
     *
     * @param costDate Date the current cost is due.
     * @param payDate  Date of the next payday.
     * @return the number of payPeriods before the current cost is due.
     */

    public static BigDecimal getPayPeriods(DateTime costDate, DateTime payDate) {
        BigDecimal periods = BigDecimal.ONE;
        DateTime curPay = new DateTime(payDate);
        while (compareDatesToTheDay(costDate, curPay) > 0) {
            periods = periods.add(BigDecimal.ONE);
            curPay = curPay.plusWeeks(2);
        }
        return periods;
    }

    /**
     * Returns the next payday in millisecond form.
     *
     * @param payList List of paychecks
     * @return next payday in millisecond form.
     */

    public static DateTime getNextPay(List<Pay> payList) {
        DateTime currentTime = DateTime.now();
        for (int i = 0; i < payList.size(); i++) {
            if (payList.get(i).getType().equals(BI_WEEKLY) && compareDatesToTheDay(new DateTime(payList.get(i).getDate()), DateTime.now()) > 0) {
                while (true) {
                    DateTime payDate = payList.get(i).getDate();
                    if (compareDatesToTheDay(currentTime, payDate) == 1 || compareDatesToTheDay(currentTime, payDate) == 0) {
                        payDate.plusDays(14);
                    } else {
                        return new DateTime(payDate);
                    }
                }

            }
        }
        return new DateTime(0);
    }

    /**
     * Creates a payment based on the input cost.
     *
     * @param cost       cost to be made into a payment.
     * @param payPeriods Number of payPeriods until cost is due.
     * @return a payment based on the input cost.
     */

    public static Payment prepPayment(Cost cost, BigDecimal payPeriods) {
        Payment payment = new Payment();
        payment.setName(cost.getName());
        payment.setAmount(cost.getAmount().subtract(cost.getSaved()).divide(payPeriods, BigDecimal.ROUND_HALF_UP));
        payment.setDate(DateTime.now());
        payment.setCategory(cost.getCategory());
        return payment;
    }

    public static BigDecimal addAmounts(List<BigDecimal> bigDecimals) {
        BigDecimal finalNumber = BigDecimal.ZERO;
        for (BigDecimal bigDecimal : bigDecimals.stream().filter(Objects::nonNull).collect(Collectors.toCollection(ArrayList::new))) {
            finalNumber = finalNumber.add(bigDecimal);
        }
        return (finalNumber);
    }

    public static void displayShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}
